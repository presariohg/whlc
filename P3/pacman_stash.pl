/**

pacman_template.pl

Template for solution to 3rd lab assignment of the course 'Knowledge
Representation and High-Level Control' in WS 2018/2019 at FH Aachen.

@author Jens Claßen

**/
        
:- use_module(vis).
:- use_module(layout).
:- dynamic poss/2.
:- dynamic some/2.
:- dynamic v/2.

:- [golog].

:- discontiguous(at/3).
:- discontiguous(scared/1).

% Primitive control actions

primitive_action(move(_Direction)).  % Move in direction Dir.
primitive_action(stop).        % Do nothing.

% Definitions of Complex Control Actions


% [YOUR CODE HERE]
proc(move(Direction)). % eat/die if possible
%% proc(go(Direction), while(poss(move(Direction)), move(Direction))).
% proc(walk(Path),...). % then stop
% proc(eat(Item),...).
%% proc(main, while(some(n, at(n))) : stop).


% Preconditions for Primitive Actions.


% [YOUR CODE HERE]
poss(move(Direction), Situation):-
    %% find pacman cell
    at(cell(Row, Col), pacman, Situation),

    %% find the adjacent cell with the given direction
    adjacent(cell(Row, Col), Direction, NextCell),

    %% check passability of the cell in the given direction
    is_passable(NextCell).

    %% eat(NextCell, Situation).

poss(stop, _Situation).


% Successor Situation Axioms for Primitive Fluents.

at(Cell, Content, do(A, Situation)) :-
    A = move(Direction);
    A = stop(),

    at(Cell, Content, Situation).

scared(do(A,Situation)) :-
    scared(Situation).


% [YOUR CODE HERE]

eat(Cell, Situation):-
    %% eat if this cell contains
    eat_capsule(Cell, Situation);
    eat_food(Cell, Situation);
    eat_ghost(Cell, Situation);
    true.

eat_capsule(Cell, Situation):-
    at(Cell, capsule),
    retract(at(Cell, capsule)),
    assert(scared(Situation)).

eat_food(Cell, _):-
    at(Cell, food),
    retract(at(Cell, food)).

eat_ghost(Cell, Situation):-
    at(Cell, ghost),
    retract(scared(Situation)),
    scared(Situation),
    retract(at(Cell, ghost)).


% Initial Situation.

at(Cell, Content, s0) :- 
    at(Cell, Content).

scared(s0) :- fail. % false intially

% Derived predicates from sheet #2, some of which extended with
% situation argument


% [YOUR SOLUTION TO SHEET #2 HERE]
% [Note: Some predicates need situation arguments!]


% Restore suppressed situation arguments.


restoreSitArg(at(Cell, Content), Situation, at(Cell, Content, Situation)).
restoreSitArg(scared, Situation, scared(Situation)).
%% restoreSitArg(go(Direction), Situation, go(Direction, Situation)).
%% restoreSitArg(move(Direction), Situation, move(Direction, Situation)).
%% restoreSitArg(food_left, Situation, food_left(Situation)).
%% restoreSitArg(capsules_left, Situation, capsules_left(Situation)).
%% restoreSitArg(ghosts_left, Situation, ghosts_left(Situation)).
%% restoreSitArg(alive, Situation, alive(Situation)).

% Use this predicate to test your procedure
% L = layout file, P = procedure to test
test(Layout, Procedure) :- load_layout(Layout), do(Procedure, s0, Situation), execute(Situation, Layout).
