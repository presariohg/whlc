/**

pacman_template.pl

Template for solution to 3rd lab assignment of the course 'Knowledge
Representation and High-Level Control' in WS 2018/2019 at FH Aachen.

@author Jens Claßen

**/
        
:- use_module(vis).
:- use_module(layout).
:- dynamic poss/2.
:- dynamic some/2.
:- dynamic v/2.

:- [golog].

:- discontiguous(at/3).
:- discontiguous(scared/1).
:- discontiguous(eat/1).

% Primitive control actions

primitive_action(move(_Direction)).  % Move in direction Dir.
primitive_action(stop).        % Do nothing.

% Definitions of Complex Control Actions


% [YOUR CODE HERE]
proc(go(Direction), 
        while(poss(move(Direction)),
            move(Direction)
        ):

        stop).
% proc(walk(Path),...). % then stop
% proc(eat(Item),...).
proc(main, 
    go(west)).

% Preconditions for Primitive Actions.


% [YOUR CODE HERE]
poss(move(Direction), Situation):-
    %% find pacman cell
    at(CurrentCell, pacman, Situation),
    %% find the adjacent cell with the given direction
    adjacent(CurrentCell, Direction, NextCell),

    %% check passability of the cell in the given direction
    is_passable(NextCell).

poss(stop, _State):-
    true.


% Successor Situation Axioms for Primitive Fluents.
at(Cell, Content, do(Action, Situation)) :-
    %% find previous pacman cell
    at(PrevCell, Content, Situation),
    %% check if this cell is in the right direction
    adjacent(PrevCell, Direction,  Cell),

    %% A must be the action move() in that direction.
    Action = move(Direction).

scared(do(Action, Situation)) :-
    %% find current pacman cell
    at(CurrentCell, pacman, do(Action, Situation)),
    %% check if this cell contain a capsule in the previous situation
    at(CurrentCell, capsule, Situation).


%% % [YOUR CODE HERE]
% Initial Situation.

at(Cell, Content, s0) :- 
    at(Cell, Content).
scared(s0) :- fail. % false intially

% Derived predicates from sheet #2, some of which extended with
% situation argument


% [YOUR SOLUTION TO SHEET #2 HERE]
% [Note: Some predicates need situation arguments!]


% Restore suppressed situation arguments.

restoreSituationArguments(at(Cell, Content), Situation, at(Cell, Content, Situation)).
restoreSituationArguments(scared, Situation, scared(Situation)).
%% restoreSituationArguments(food_left, Situation, food_left(Situation)).
%% restoreSituationArguments(capsules_left, Situation, capsules_left(Situation)).
%% restoreSituationArguments(ghosts_left, Situation, ghosts_left(Situation)).
%% restoreSituationArguments(alive, Situation, alive(Situation)).

% Use this predicate to test your procedure
% L = layout file, P = procedure to test
test(Layout, Procedure) :- load_layout(Layout), do(Procedure, s0, Situation), execute(Situation, Layout).
%% test("testMaze.lay", main).