is_male(george).
is_male(philip).
is_male(spence).
is_male(charles).
is_male(mark).
is_male(andrew).
is_male(edward).
is_male(william).
is_male(harry).
is_male(peter).
is_male(mike).
is_male(eugenie).
is_male(james).
is_male(georgeII).
is_male(louis).
is_male(archie).


is_female(mum).
is_female(elizabeth).
is_female(margaret).
is_female(kydd).
is_female(diana).
is_female(anne).
is_female(sarah).
is_female(sophie).
is_female(kate).
is_female(meghan).
is_female(autumn).
is_female(zara).
is_female(beatrice).
is_female(louise).
is_female(charlotte).
is_female(savannah).
is_female(mia).
is_female(grace).

%% are_parents(parent1, parent2, child)
are_parents(george, mum, elizabeth).            
are_parents(george, mum, margaret).       

are_parents(philip, elizabeth, charles).
are_parents(philip, elizabeth, anne).
are_parents(philip, elizabeth, andrew).
are_parents(philip, elizabeth, edward).

are_parents(kydd, spence, diana).

are_parents(charles, diana, william).
are_parents(charles, diana, harry).

are_parents(mark, anne, peter).
are_parents(mark, anne, zara).

are_parents(sarah, andrew, beatrice).
are_parents(sarah, andrew, eugenie).

are_parents(sophie, edward, louise).
are_parents(sophie, edward, james).

are_parents(william, kate, georgeII).
are_parents(william, kate, charlotte).
are_parents(william, kate, louis).

are_parents(harry, meghan, archie).

are_parents(autumn, peter, savannah).
are_parents(autumn, peter, isla).

are_parents(zara, mike, mia).
are_parents(zara, mike, grace).


is_parent(Parent, Me):-
    are_parents(Parent, _, Me);
    are_parents(_, Parent, Me).

is_father(Father, Me):-
    is_parent(Father, Me),
    is_male(Father).

is_mother(Mother, Me):-
    is_parent(Mother, Me),
    is_female(Mother).

%% Spouses have at least 1 mutual child, doesn't work if the couple doesn't have at least 1 common child lol
is_spouse(Spouse, Me):-
    is_parent(Spouse, Child),
    is_parent(Me, Child),
    Spouse \= Me.

is_husband(Husband, Me):-
    is_spouse(Husband, Me),
    is_male(Husband).

is_wife(Wife, Me):-
    is_spouse(Wife, Me),
    is_female(Wife).


%% Parent in law is spouse's parent
is_parent_in_law(ParentInLaw, Me):-
    is_parent(ParentInLaw, Spouse),
    is_spouse(Me, Spouse).


%% Father in law is the male parent in law
is_father_in_law(FatherInLaw, Me):-
    is_parent_in_law(FatherInLaw, Me),
    is_male(FatherInLaw).


%% Mother in law is the femal parent in law
is_mother_in_law(MotherInLaw, Me):-
    is_parent_in_law(MotherInLaw, Me),
    is_female(MotherInLaw).


%% Grandparent is parent's parent
is_grandparent(Grandparent, Child):-
    is_parent(Grandparent, Parent),
    is_parent(Parent, Child).

is_grandparent_in_law(GrandparentInLaw, Me):-
    is_grandparent(GrandparentInLaw, Spouse),
    is_spouse(Spouse, Me).


%% GreatGrandParent is Grandparent's parent
is_great_grandparent(GreatGrandParent, Me):-
    is_parent(GreatGrandParent, Grandparent),
    is_grandparent(Grandparent, Me).

is_great_grandchild(GreatGrandChild, Me):-
    is_great_grandparent(Me, GreatGrandChild).

%% Grandchild is Grandparent's child
is_grandchild(GrandChild, Me):-
    is_grandparent(Me, GrandChild).


%% Ancestor is someone somewhere above Me, recursively
is_ancestor(Parent, Me):-
    is_parent(Parent, Me).

is_ancestor(Ancestor, Me):-
    is_parent(Parent, Me),
    is_ancestor(Ancestor, Parent).


%% My siblings and I have the same parent
are_siblings(Sibling, Me):-
    is_parent(Parent, Me),
    is_parent(Parent, Sibling),
    Sibling \= Me.


%% Sister is one's female siblings
is_sister(Sister, Me):-
    are_siblings(Sister, Me),
    is_female(Sister).


%% Brother is one's male siblings
is_brother(Brother, Me):-
    are_siblings(Brother, Me),
    is_male(Brother).


%% Daughter is Parent's female offspring
is_daughter(Daughter, Me):-
    is_parent(Me, Daughter),
    is_female(Daughter).

is_daughter_in_law(DaughterInLaw, Me):-
    is_wife(DaughterInLaw, MyChild),
    is_parent(Me, MyChild).

%% Son is parent's male offspring
is_son(Son, Parent):-
    is_parent(Parent, Son),
    is_male(Son).

is_son_in_law(SonInLaw, Me):-
    is_husband(SonInLaw, MyChild),
    is_parent(Me, MyChild).

%% First cousins share at least on common grandparent
is_first_causin(Me, Cousin):-
    is_grandparent(Grandparent, Me),
    is_grandparent(Grandparent, Cousin),
    Me \= Cousin.


%% Brother in law is my spouse's brother, or my sibling's husband
is_brother_in_law(BrotherInLaw, Me):-
    is_male(BrotherInLaw),
    are_siblings(Me, Sibling),
    is_spouse(BrotherInLaw, Sibling).

is_brother_in_law(BrotherInLaw, Me):-
    is_male(BrotherInLaw),
    are_siblings(BrotherInLaw, MySpouse),
    is_spouse(Me, MySpouse).


%% Sister in law is my spouse's sister, or my sibling's wife
is_sister_in_law(SisterInLaw, Me):-
    is_female(SisterInLaw),
    are_siblings(Me, Sibling),
    is_spouse(SisterInLaw, Sibling).

is_sister_in_law(SisterInLaw, Me):-
    is_female(SisterInLaw),
    are_siblings(SisterInLaw, MySpouse),
    is_spouse(Me, MySpouse).


%% Aunt is parent's sister
is_aunt(Aunt, Me):-
    is_sister(Aunt, Parent),
    is_parent(Parent, Me).

% Uncle is parent's brother
is_uncle(Uncle, Me):-
    is_brother(Uncle, Parent),
    is_parent(Parent, Me).


%% Nephew is sibling's son
is_nephew(Nephew, Me):-
    are_siblings(MySibling, Me),
    is_son(Nephew, MySibling).


%% Niece is sibling's daughter
is_niece(Niece, Me):-
    are_siblings(MySibling, Me),
    is_daughter(Niece, MySibling).


relation(Relation, Person1, Person2):-
    member(Relation, [is_parent, is_grandparent, is_great_grandparent, is_ancestor, %% Above me, blood related
                      is_uncle, is_aunt, is_father, is_mother,                      %% Above me, blood related

                      is_parent_in_law, is_grandparent_in_law, is_father_in_law,    %% Above me, not blood related
                      is_mother_in_law,                                             %% Above me, not blood related

                      are_siblings, is_sister, is_brother, is_first_causin,         %% Equal me, blood related

                      is_spouse, is_brother_in_law, is_sister_in_law, is_husband,   %% Equal me, not blood related 
                      is_wife,                                                      %% Equal me, not blood related 

                      is_son, is_daughter, is_niece, is_nephew, is_grandchild,      %% Under me, blood related
                      is_great_grandchild,                                          %% Under me, blood related 

                      is_son_in_law, is_daughter_in_law]),                          %% Under me, not blood related 
    call(Relation, Person1, Person2).
